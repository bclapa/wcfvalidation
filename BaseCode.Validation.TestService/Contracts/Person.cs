﻿using System.Runtime.Serialization;

namespace BaseCode.Validation.TestService.Contracts
{
  [DataContract]
  public class Person
  {
    [DataMember]
    public string FirstName { get; set; }

    [DataMember]
    public string LastName { get; set; }

    [DataMember]
    public Address Address { get; set; }
  }
}