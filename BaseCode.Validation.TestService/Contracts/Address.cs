﻿using System.Runtime.Serialization;

namespace BaseCode.Validation.TestService.Contracts
{
  [DataContract]
  public class Address
  {
    [DataMember]
    public string Street { get; set; }
    [DataMember]
    public string StreetNumber { get; set; }
    [DataMember]
    public string FlatNumber { get; set; }
    [DataMember]
    public string City { get; set; }
  }

  [DataContract]
  public class TestClass
  {
    
  }
}