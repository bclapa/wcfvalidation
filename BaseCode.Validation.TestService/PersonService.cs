﻿using BaseCode.Validation.Core;
using BaseCode.Validation.TestService.Contracts;

namespace BaseCode.Validation.TestService
{
  [WcfValidatorServiceBehavior]
  public class PersonService : IPersonService
  {
    public void AddPerson(Person person)
    {
      
    }

    public string Echo(string data)
    {
      return $"Received Data: {data}";
    }
  }
}
