using BaseCode.Validation.Core;
using BaseCode.Validation.TestService.Contracts;
using FluentValidation;

namespace BaseCode.Validation.TestService.Validators
{
  public class AddressValidator : AbstractWcfValidator<Address>
  {
    public AddressValidator()
    {
      RuleFor(x => x.Street).NotEmpty().Length(1, 50);
      RuleFor(x => x.StreetNumber).NotEmpty().Length(1, 50);
      RuleFor(x => x.FlatNumber).NotEmpty().Length(1, 50);
      RuleFor(x => x.City).NotEmpty().Length(1, 50);
    }
  }

  public class TestClassValidator : AbstractWcfValidator<TestClass>
  {
    
  }
}