﻿using BaseCode.Validation.Core;
using BaseCode.Validation.TestService.Contracts;
using FluentValidation;

namespace BaseCode.Validation.TestService.Validators
{
  public class PersonValidator : AbstractWcfValidator<Person>
  {
    public PersonValidator()
    {
      RuleFor(x => x.FirstName).NotEmpty().Length(1, 50);
      RuleFor(x => x.LastName).NotEmpty().Length(1, 50);
      RuleFor(x => x.Address).NotNull().SetValidator(new AddressValidator());
    }
  }
}