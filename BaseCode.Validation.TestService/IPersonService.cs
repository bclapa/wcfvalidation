﻿using System.ServiceModel;
using BaseCode.Validation.Core;
using BaseCode.Validation.Core.Contracts;
using BaseCode.Validation.TestService.Contracts;

namespace BaseCode.Validation.TestService
{
  [ServiceContract]
  public interface IPersonService
  {
    [OperationContract]
    [FaultContract(typeof(ValidationFault))]
    void AddPerson(Person person);

    [OperationContract]
    string Echo(string data);
  }

  [ServiceContract]
  public interface ITestService
  {
    [OperationContract]
    [FaultContract(typeof(ValidationFault))]
    void Test(TestClass data);
  }

  [WcfValidatorServiceBehavior]
  public class TestService : ITestService
  {
    public void Test(TestClass data)
    {
      
    }
  }
}
