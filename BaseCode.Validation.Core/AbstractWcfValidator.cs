﻿using System;
using FluentValidation;

namespace BaseCode.Validation.Core
{
  public abstract class AbstractWcfValidator<T> : AbstractValidator<T>, IWcfValidator
  {
    public Type ValidatedObjectType => typeof(T);
  }
}