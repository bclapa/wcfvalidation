using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using BaseCode.Validation.Core.Contracts;
using FluentValidation.Results;

namespace BaseCode.Validation.Core
{
  public class WcfValidatorParameterInspector : IParameterInspector
  {
    public object BeforeCall(string operationName, object[] inputs)
    {
      if (inputs != null)
      {
        foreach (var input in inputs)
        {
          if (input != null)
          {
            var inputType = input.GetType();
            var validator = WcfValidationRecords.GetValidator(inputType);
            if (validator != null)
            {
              var result = validator.Validate(input);
              if (!result.IsValid)
              {
                var fault = new ValidationFault();
                fault.ValidationDetails =
                  result.Errors.Select(
                    x => new ValidationDetail() { PropertyName = x.PropertyName, ValidationMessage = x.ErrorMessage })
                    .ToList();

                throw new FaultException<ValidationFault>(fault);
              }
            }
          }
        }
      }

      return null;
    }

    public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
    {
    }
  }
}