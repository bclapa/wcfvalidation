﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BaseCode.Validation.Core.Contracts
{
  [DataContract]
  public class ValidationFault
  {
    [DataMember]
    public List<ValidationDetail> ValidationDetails { get; set; }

    public ValidationFault()
    {
      ValidationDetails = new List<ValidationDetail>();
    }
  }
}