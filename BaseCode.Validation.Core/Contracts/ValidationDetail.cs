﻿using System.Runtime.Serialization;

namespace BaseCode.Validation.Core.Contracts
{
  [DataContract]
  public class ValidationDetail
  {
    [DataMember]
    public string PropertyName { get; set; }
    [DataMember]
    public string ValidationMessage { get; set; }
  }
}