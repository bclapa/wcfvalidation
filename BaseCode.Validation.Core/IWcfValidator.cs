﻿using System;
using FluentValidation;

namespace BaseCode.Validation.Core
{
  public interface IWcfValidator : IValidator
  {
    Type ValidatedObjectType { get; }
  }
}