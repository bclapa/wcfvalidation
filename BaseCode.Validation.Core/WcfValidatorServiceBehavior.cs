﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace BaseCode.Validation.Core
{
  [AttributeUsage(AttributeTargets.Class)]
  public class WcfValidatorServiceBehavior : Attribute, IServiceBehavior
  {
    private readonly WcfValidationRecords _validationRecords;
    private readonly List<string> _processedAssemblies;

    public WcfValidatorServiceBehavior()
    {
      _validationRecords = new WcfValidationRecords();
      _processedAssemblies = new List<string>();
    }

    public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
    }

    public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
      BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      RegisterValidators(serviceDescription.ServiceType);

      var parameterInspector = new WcfValidatorParameterInspector();
      foreach (var channelDispatcherBase in serviceHostBase.ChannelDispatchers)
      {
        var cd = (ChannelDispatcher)channelDispatcherBase;
        foreach (var ed in cd.Endpoints)
        {
          if (ed.IsSystemEndpoint)
            continue;

          foreach (var dispatchOperation in ed.DispatchRuntime.Operations)
          {
            dispatchOperation.ParameterInspectors.Add(parameterInspector);
          }
        }
      }
    }

    private void RegisterValidators(Type type)
    {
      if (_processedAssemblies.Contains(type.Assembly.FullName))
        return;

      _processedAssemblies.Add(type.Assembly.FullName);

      var assemblyTypes = type.Assembly.GetTypes();
      var validators = assemblyTypes.Where(t => typeof(IWcfValidator).IsAssignableFrom(t)).ToList();
      foreach (var validator in validators)
      {
        var v = Activator.CreateInstance(validator) as IWcfValidator;
        if (v != null)
        {
          _validationRecords.AddValidator(v.ValidatedObjectType, validator);
        }
      }
    }
  }
}