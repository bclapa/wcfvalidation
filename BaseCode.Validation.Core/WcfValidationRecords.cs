﻿using System;
using System.Collections.Generic;

namespace BaseCode.Validation.Core
{
  public class WcfValidationRecords
  {
    private static readonly Dictionary<Type, Type> Records = new Dictionary<Type, Type>();

    public static IWcfValidator GetValidator(Type type)
    {
      Type validatorType;
      if (Records.TryGetValue(type, out validatorType))
      {
        return Activator.CreateInstance(validatorType) as IWcfValidator;
      }

      return null;
    }

    public void AddValidator(Type objectType, Type objectValidator)
    {
      if (!Records.ContainsKey(objectType))
      {
        Records.Add(objectType, objectValidator);
      }
    }
  }
}